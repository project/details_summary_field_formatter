CONTENTS OF THIS FILE
---------------------

 * INTRODUCTION
 * REQUIREMENTS
 * INSTALLATION
 * CONFIGURATION


INTRODUCTION
------------
Provides a field formatter in details with summary tag.
This field formatter supports in the below field types
     "text",
      "text_long",
      "text_with_summary",

 * For a full description of the module visit:
   https://www.drupal.org/project/details_summary_field_formatter

 * To submit bug reports and feature suggestions, or to track changes visit:
   https://www.drupal.org/project/issues/details_summary_field_formatter


REQUIREMENTS
------------

This module requires no modules outside of Drupal core.


INSTALLATION
------------

 * Install the module as you would normally install a contributed
   Drupal module. Visit https://www.drupal.org/node/1897420 for further
   information.
 * Recommended: Install with Composer:
   composer require 'drupal/details_summary_field_formatter'

CONFIGURATION
-------------

  * On `admin/structure/types` choose the content type you want to use for
    deatils_summary formatter, for example *Article*.
  * Select **Manage Display** or go to
    `admin/structure/types/manage/article/display`.
  * Choose a field you want to use and in formatter settings
    define **Details with Summary** as the format.
  * In formatter settings you can specify whether it should be expanded or not.
    by enabling Expanded Checkbox.
    You can also specify the summary tag label in Custom Summary textfield,
    By default it will take field label.
