<?php

namespace Drupal\details_summary_field_formatter\Plugin\Field\FieldFormatter;

use Drupal\Core\Field\FormatterBase;
use Drupal\Core\Field\FieldItemListInterface;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Component\Utility\Xss;

/**
 * Plugin implementation of the 'details_summary_field_formatter' formatter.
 *
 * @FieldFormatter(
 *   id = "details_summary_field_formatter",
 *   label = @Translation("Details with Summary"),
 *   field_types = {
 *     "text",
 *     "text_long",
 *     "text_with_summary",
 *   }
 * )
 */
class DetailsSummaryFormatter extends FormatterBase {

  /**
   * {@inheritdoc}
   */
  public static function defaultSettings() {
    return [
      'expanded' => TRUE,
      'custom_summary' => '',
    ] + parent::defaultSettings();
  }

  /**
   * {@inheritdoc}
   */
  public function settingsForm(array $form, FormStateInterface $form_state) {

    $form['expanded'] = [
      '#title' => $this->t('Expanded'),
      '#description' => $this->t('By default expanded.'),
      '#type' => 'checkbox',
      '#default_value' => $this->getSetting('expanded'),
    ];

    $form['custom_summary'] = [
      '#title' => $this->t('Custom Summary'),
      '#type' => 'textfield',
      '#default_value' => $this->getSetting('custom_summary'),
      '#placeholder' => $this->fieldDefinition->getLabel(),
      '#description' => $this->t('Custom summary text. By default field label will be used.'),
    ];

    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function settingsSummary() {
    $summary = [];

    if ($this->getSetting('expanded')) {
      $summary[] = $this->t('Detail summary : Expanded');
    }
    else {
      $summary[] = $this->t('Detail summary: Collapsed');
    }

    if ($this->getSetting('custom_summary')) {
      $summary[] = $this->t('Custom Summary : @custom_summary',
        ['@custom_summary' => Xss::filterAdmin($this->getSetting('custom_summary'))]
      );
    }
    else {
      // Use the field label as a fallback.
      $summary[] = $this->t('Label Summary : @label',
        ['@label' => $this->fieldDefinition->getLabel()]
      );
    }

    return $summary;
  }

  /**
   * {@inheritdoc}
   */
  public function viewElements(FieldItemListInterface $items, $langcode) {
    $elements = [];

    foreach ($items as $delta => $item) {
      if ($this->getSetting('custom_summary')) {
        $custom_summary = Xss::filterAdmin($this->getSetting('custom_summary'));
      }
      else {
        // Use the field label as a fallback.
        $custom_summary = $this->fieldDefinition->getLabel();
      }

      $elements[$delta] = [
        '#type' => 'details',
        '#open' => (bool) $this->getSetting('expanded'),
        '#title' => $custom_summary,
      ];
      $elements[$delta]['content'] = [
        '#type' => 'processed_text',
        '#text' => $item->value,
        '#format' => $item->format,
        '#langcode' => $item->getLangcode(),
      ];

    }

    return $elements;
  }

}
